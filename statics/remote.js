let start = async () => {
  let response = await fetch('/start')
  if (response.ok) {
    let text = await response.text()
    console.log(text)
  } else {
    console.log("HTTP-Error: " + response.status)
  }
}
