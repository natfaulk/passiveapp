const path = require('path')
const moment = require('moment')
const savefiles = require('./savefiles.js')
const APP_VERSION = require('./package.json').version;

const express = require('express')
const port = 3001
const ex_app = express()

let app = angular.module('myApp', [])
app.controller('passive', ['$scope', '$http', '$timeout', '$interval', function($s, $http, $timeout, $interval) {
  $s.VERSION = APP_VERSION
  savefiles.init()
  $s.devices = []
  $s.filestore = {}
  $s.fileprefix = ''
  $s.countdown = 0
  $s.nsamples = 900

  $interval(() => {
    if ($s.countdown > 0) $s.countdown--
  }, 1000)

  ex_app.use(express.static('statics'))
  ex_app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'statics', 'remote.html')))
  ex_app.get('/start', (req, res) => {
    if ($s.countdown === 0) {
      $s.collectall()
    }

    res.send('ack')
  })

  ex_app.listen(port, '0.0.0.0', () => console.log(`Example app listening on port ${port}!`))

  $s.collectall = () => {
    $s.message = ''
    $s.filestore = {
      count: $s.devices.length,
      succeeded: 0,
      failed: 0
    }

    $s.countdown = $s.nsamples / 10 + 2

    $s.devices.forEach(element => {
      $s.collect(element, res => {
        if (res) {
          element.message = 'Collection succeeded'
          $s.filestore.succeeded++
        } else {
          element.message = 'Collection failed'
          $s.filestore.failed++
        }

        if ($s.filestore.succeeded + $s.filestore.failed == $s.filestore.count) {
          if ($s.filestore.failed == 0) {
            let out = {VERSION: $s.VERSION, time: Date.now()}
            $s.devices.forEach(element => {
              out[element.name] = element.data
            })
            let i = savefiles.nextFile(out, $s.fileprefix)
            let tempPrefix = $s.fileprefix
            if (tempPrefix != '') tempPrefix+= '_'
            $s.message = `Saved file: ${tempPrefix}${i}.json`
          } else {
            $s.message = `Failed to save data`
          }
        }
      })
    })
  }

  $s.collect = (_device, _callback) => {
    $s.dev_begin(_device.ip, (err, data) => {
      if (err) {
        console.log('Error: ' + err.message)
        _callback(false)
      } else if (data == 'Ack') {
        console.log('got ack')
        _device.message = 'Collecting...'
        // should take 10 seconds, timeout for 12 in case
        $timeout(() => {
          $s.dev_check(_device.ip, (err, data) => {
            if (err) {
              console.log('Error: ' + err.message)
              _callback(false)
            } else if (data == 'true') {
              $s.dev_get(_device.ip, (err, data) => {
                if (err) {
                  console.log('Error: ' + err.message)
                  _callback(false)
                } else {
                  _device.data = data
                  _callback(true)
                }
              })
            } else {
              console.log('not true')
              _callback(false)
            }
          })
        }, $s.nsamples * 100 + 2000)
      } else {
        _callback(false)
        console.log('not ack')
      }
    })
  }

  $s.getReq = (_host, _path, _callback) => {
    $http.get(`http://${_host}/${_path}`).then((res) => {
      _callback(null, res.data)
    }, (err) => {
      _callback(err, null)
    })
  }

  $s.getReq('passivevlp.natfaulk.com', 'getdevices', (err, data) => {
    if (err) console.log('Error: ' + err.message)
    else {
      $s.devices = data
      $s.devices.forEach(element => {
        element.data = []
        element.message = ''
        element.seenpretty = moment(element.seen).fromNow()
      })
      $s.devices.sort((a, b) => {
        if (a.name > b.name) return 1
        else if (a.name < b.name) return -1
        else return 0
      })
      console.log($s.devices)
    }
  })

  $s.dev_begin = (_ip, _callback) => {
    $s.getReq(_ip, `begin?nsamples=${$s.nsamples}`, (err, data) => {
      _callback(err, data)
    })
  }

  $s.dev_check = (_ip, _callback) => {
    $s.getReq(_ip, 'checkdone', (err, data) => {
      _callback(err, data)
    })
  }

  $s.dev_get = (_ip, _callback) => {
    $s.getReq(_ip, 'getdata', (err, data) => {
      _callback(err, data)
    })
  }
}])

