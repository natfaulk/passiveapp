# VLP App

## Install instructions
- Download and install nodejs from [here](https://nodejs.org/en/download/current/). (Get the installer package not the binary package)
- Download and install git from [here](https://git-scm.com/downloads)
- Open a command prompt or powershell and download the files: `git clone https://natfaulk@bitbucket.org/natfaulk/passiveapp.git` 
- Navigate to the downloaded folder: `cd passiveapp`
- Install the dependencies: `npm install`

## Run the app
`npm start`

## To update
- open a command prompt in the folder containing the install files (easiest way is to go to the folder in windows explorer then using the menus `File -> Open Windows Powershell`)
- Run `git pull`
- Delete the node_modules folder using windows explorer (not always necessary but reccomended)
- Run `npm install`

## Other things to note
If the app freezes or does something strange, one can reload the app by pressing `Ctrl r`  

The saved files are stored in the outputs folder. They are saved as json files.  
The format is:  
{  
  "VERSION": "version number e.g 1.1.0",
  "pd_1": [pd1 data as a list.....],  
  "pd_2": [pd2 data as a list.....],
  etc...  
}  

If you want to process the data use javascript or a JSON library for another language to parse and load the data - it will make your life much easier! (Matlab has a json loader)